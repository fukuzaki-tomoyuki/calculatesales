package jp.alhinc.fukuzaki_tomoyuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {
		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> salesmap = new HashMap<String, Long>();
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if (read(args[0], "branch.lst", branchmap, salesmap)) {
		} else {
			return;
		}
		//売上集計処理
		BufferedReader rcd = null;
		String fileName = null;
		List<Integer> numberlist = new ArrayList<>();
		List<String> ext = new ArrayList<>();
		File filename = new File(args[0]);
		File[] fileNameList = filename.listFiles();
		for (int i = 0; i < fileNameList.length; ++i) {
			fileName = fileNameList[i].getName();
			//ファイル名 || ファイルかどうか
			if (fileName.matches("[0-9]{8}.rcd$") && fileNameList[i].isFile()) {
				ext.add(fileName);
				//先頭の0個目から8個目までの名前取得
				String fileNumber = fileName.substring(0, 8);
				//Integerにキャスト 演算可能にする
				int Number = Integer.parseInt(fileNumber);
				numberlist.add(Number);
			}
		}
		//連番判定
		for (int j = 0; j < numberlist.size() - 1; ++j) {
			if (numberlist.get(j + 1) - numberlist.get(j) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//集計計算
		try {
			for (int j = 0; j < ext.size(); ++j) {
				List<String> linelist = new ArrayList<>();
				File file = new File(args[0], ext.get(j));
				rcd = new BufferedReader(new FileReader(file));
				//1行目から読み込み
				String line = null;
				while ((line = rcd.readLine()) != null) {
					linelist.add(line);
				}
				//2行じゃない場合
				if (!(linelist.size() == 2)) {
					System.out.println(ext.get(j) + "のフォーマットが不正です");
					return;
				}
				if (!(linelist.get(0).matches("^[0-9]*$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if (!(linelist.get(1).matches("^[0-9]*$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				String key = linelist.get(0);
				//リストの一つ目とsalesmapのkeyが一緒だったら
				if (!salesmap.containsKey(key)) {
					System.out.println(ext.get(j) + "の支店コードが不正です");
					return;
				}
				//keyでvalueを取得
				Long value = salesmap.get(key);
				//String型で保持された集計データを取り出す
				Long total1 = value;
				//キャスト,売上合計データ先
				Long total2 = Long.parseLong(linelist.get(1));
				//キャスト,一日の売上データ
				Long total = total1 + total2;
				//合計金額が10桁を超えた時のエラー処理
				if (10000000000L <= total) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//System.out.println(total);
				//合計金額をsalesmapに上書き
				salesmap.put(key, total);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			try {
				if (rcd != null)
					rcd.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}
		if (out(args[0], "branch.out", branchmap, salesmap)) {
		} else {
			return;
		}
	}

	public static boolean out(String arg01, String arg02, HashMap<String, String> branch, HashMap<String, Long> sales) {
		//ファイル出力処理
		BufferedWriter bw = null;
		try {
			//ストリーム
			File file = new File(arg01, arg02);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			//keyとvalueの取り出し
			Iterator<String> key_itr = branch.keySet().iterator();
			//hasNextで値がある場合にループ処理
			while (key_itr.hasNext()) {
				String key = (String) key_itr.next();
				//System.out.println(key);
				//System.out.println(key + "," + branchmap.get(key) + "," + salesmap.get(key));
				bw.write(key + "," + branch.get(key) + "," + sales.get(key)
						+ System.getProperty("line.separator"));
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (bw != null)
					bw.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

	public static boolean read(String arg01, String arg02, HashMap<String, String> branch,
			HashMap<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(arg01,arg02);
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			// FileReader 変数 = new FileReader(file);↓
			//BuffferedReader br = new BufferedReader(変数);の略
			br = new BufferedReader(new FileReader(file));
			String str;
			while ((str = br.readLine()) != null) {
				String[] branchlist = str.split(",");
				if (!(branchlist.length == 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branch.put(branchlist[0], branchlist[1]);
				//branch.lstのフォーマット確認
				//[0]が0～9が3桁続き
				if (!branchlist[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//(支店番号、集計場所)のMapを用意
				//数値だけだとint型なので 数値L でLong型にキャスト
				sales.put(branchlist[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			// 4.最後にファイルを閉じてリソースを開放する
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}
